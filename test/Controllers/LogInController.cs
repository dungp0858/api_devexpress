﻿using Azure.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Resources;
using System;
using DB;
using test.Common;
using Newtonsoft.Json.Linq;
using static test.Common.Baseclass;

namespace test.Controllers
{
    [Consumes("application/json")]
    [Route("api/HeThong/dangnhap")]
    [ApiController]
    public class LogInController : BaseController
    {
        public LogInController(DB_CONTEXT db) : base(db) { }

        [HttpPost]
        public IActionResult GetAuthToken([FromBody] JObject item)
        {
            NguoiDung objHT_NGUOIDUNG = _db.NguoiDungCollection.GetByTenDangNhap(item["TEN_DANG_NHAP"].Value<string>().Trim());
             if (objHT_NGUOIDUNG != null)
             {
                    if (ConvertClass.GIAI_MA_HOA_MK(objHT_NGUOIDUNG.MAT_KHAU) == item["MAT_KHAU"].Value<string>().Trim())
                    {
                        DateTime requestAt = DateTime.Now;
                        DateTime expiresIn = requestAt + TokenAuthOption.ExpiresSpan;
                        string token = GenerateToken(objHT_NGUOIDUNG, expiresIn);
                        return ObjectResult(new
                        {
                            requertAt = requestAt,
                            expiresIn = TokenAuthOption.ExpiresSpan.TotalSeconds,
                            tokeyType = TokenAuthOption.TokenType,
                            accessToken = token,
                            objHT_NGUOIDUNG.TEN_DANG_NHAP,
                            objHT_NGUOIDUNG.TEN_NGUOI_DUNG,
                            objHT_NGUOIDUNG.EMAIL
                        });
                    }
            }
            return ObjectResult(null, "Tài khoản hoặc mật khẩu không đúng", RequestState.Failed);
        }
    }
}
