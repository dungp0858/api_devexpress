﻿using Azure.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Resources;
using System;
using DB;
using test.Common;
using Newtonsoft.Json.Linq;

namespace test.Controllers
{
    [Consumes("application/json")]
    [Route("api/HeThong/NguoiDung")]
    [ApiController]
    public class NguoiDungController : BaseController
    {
        public NguoiDungController(DB_CONTEXT db) : base(db){}

        //public static List<QUYEN> Permission()
        //{
        //    DM_CHUCDANHController mn = new DM_CHUCDANHController(null);
        //    return mn.QuyenCoBan();
        //}

        [HttpGet("{op}")]
        [Authorize("Bearer")]
        public IActionResult Get(string op)
        {
            //if (op == "Access")
            //{
            //    return ObjectResult(new
            //    {
            //        View = UserAccess(Quyen.Xem),
            //        New = UserAccess(Quyen.Tạo_Mới),
            //        Edit = UserAccess(Quyen.Sửa),
            //        Delete = UserAccess(Quyen.Xoá),
            //        Title = CurrentMenu?.NAME + "",
            //    });
            //}
            if (op == "List")
            {
                return ListAll(_db.NguoiDungCollection.Get());
            }
            return new BadRequestResult();
        }

        [HttpPost("{op}")]
        [Authorize("Bearer")]
        public IActionResult Post(string op, [FromBody] JObject item)
        {
            if (item == null || op == "") return new BadRequestResult();
            if (op == "Create")
            {
                var checktendangnhap = _db.NguoiDungCollection.GetByTenDangNhap(item["TEN_DANG_NHAP"].Value<string>().ToString());
                if (checktendangnhap == null)
                {
                    var objNguoiDung = item.ToObject<NguoiDung>();
                    objNguoiDung.MAT_KHAU = ConvertClass.MA_HOA_MK(item["MAT_KHAU"].Value<string>());
                    _db.NguoiDungCollection.Add(objNguoiDung);
                }
                else
                {
                    return ObjectResult(null, "Tên đăng nhập này đã tồn tại", RequestState.Failed);
                }
              
            }
            else if (op == "Delete")
            {
                
            }

            return new NoContentResult();
        }
        //edit
        [HttpPut("{ID}")]
        [Authorize("Bearer")]
        public IActionResult PutEdit(decimal ID, [FromBody] JObject item)
        {
           
            return new NoContentResult();
        }
    }
}
