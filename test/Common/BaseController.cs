﻿using DB;
using static test.Common.Baseclass;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNetCore.Identity;

namespace test.Common
{
    public class BaseController : Controller
    {
        protected readonly DB_CONTEXT _db;
        public BaseController(DB_CONTEXT db)
        {
            _db = db;
        }
        protected IActionResult ListAll(IQueryable<object> items)
        {
            return ListAlls(items, new string[] { });
        }
        protected IActionResult ListAlls(IQueryable<object> items,string[] acFields)
        {
            return ObjectResult(ListAllItemss(items));
        }

        protected QueryResult ListAllItemss(IQueryable<object> items)
        {
            return ListAllItems(items, new string[] { });
        }
        protected QueryResult ListAllItems(IQueryable<object> items, string[] acFields)
        {
            int totalCount = 0;
            //string mid = Request.Query["mid"];
            if (items == null) return new QueryResult { totalCount = 0, items = new List<object>() };
            string requireTotalCount = Request.Query["requireTotalCount"];
            int skip = Convert.ToInt32(Request.Query["skip"]);
            int take = Convert.ToInt32(Request.Query["take"]);
            //phân trang
            if (requireTotalCount != null && requireTotalCount == "true") totalCount = items.Count();
            if (take > 0) items = items.Skip(skip).Take(take);
            return new QueryResult { totalCount = totalCount, items = items.ToList() };
        }
        protected ObjectResult ObjectResult(object data, string mess = "", RequestState res = RequestState.Success)
        {
            return new ObjectResult(new RequestResult
            {
                State = res,
                Data = data,
                Msg = mess,
            });
        }
        protected string GenerateToken(NguoiDung objHT_NGUOIDUNG, DateTime expires)
        {
           
            var handler = new JwtSecurityTokenHandler();

            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(objHT_NGUOIDUNG.ID.ToString(), "TokenAuth"),
                new[] {
                    new Claim("ID", objHT_NGUOIDUNG.ID+""),
                    new Claim("TEN_NGUOI_DUNG", objHT_NGUOIDUNG.TEN_NGUOI_DUNG+""),
                    new Claim("MA_NGUOI_DUNG", objHT_NGUOIDUNG.MA_NGUOI_DUNG+""),
                    new Claim(ClaimTypes.Role,"User")
                }
            ) ;
            

            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = TokenAuthOption.Issuer,
                Audience = TokenAuthOption.Audience,
                SigningCredentials = TokenAuthOption.SigningCredentials,
                Subject = identity,
                Expires = expires
            });
            return handler.WriteToken(securityToken);
        }

    }
}
