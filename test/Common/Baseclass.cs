﻿using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;

namespace test.Common
{
    public class Baseclass
    {

        public class QueryResult
        {
            public int totalCount { get; set; }
            public List<dynamic> items { get; set; }
        }
        public class RequestResult
        {
            public RequestState State { get; set; }
            public string Msg { get; set; }
            public Object Data { get; set; }
        }

        public class Auth
        {
            public static RSAParameters GenerateKey()
            {
                using (var key = new RSACryptoServiceProvider(2048))
                {
                    return key.ExportParameters(true);
                }
            }
        }
        public class TokenAuthOption
        {
            public static string Audience { get; } = "43535";
            public static string Issuer { get; } = "345353";
            public static RsaSecurityKey Key { get; } = new RsaSecurityKey(Auth.GenerateKey());
            public static SigningCredentials SigningCredentials { get; } = new SigningCredentials(Key, SecurityAlgorithms.RsaSha256Signature);

            public static TimeSpan ExpiresSpan { get; } = TimeSpan.FromHours(24);

            public static TimeSpan IdleTimeout { get; } = TimeSpan.FromSeconds(60);
            public static string TokenType { get; } = "Bearer";
        }
    }
}
