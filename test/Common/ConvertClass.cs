﻿using System.Security.Cryptography;
using System.Text;

namespace test.Common
{
    public static class ConvertClass
    {
        public static string MA_HOA_MK(string password)
        {
            var EncodePassWord = System.Text.Encoding.UTF8.GetBytes(password);
            return System.Convert.ToBase64String(EncodePassWord);
        }
        public static string GIAI_MA_HOA_MK(string password)
        {
            var Decode = System.Convert.FromBase64String(password);
            return System.Text.Encoding.UTF8.GetString(Decode);
        }
    }
}
