﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB
{
    internal class Token
    {
        public static string Audience { get; } = "test";
        public static string Issuer { get; } = "testIssuer";
        public static TimeSpan ExpiresSpan { get; } = TimeSpan.FromHours(24);

        public static TimeSpan IdleTimeout { get; } = TimeSpan.FromSeconds(60);
        public static string TokenType { get; } = "Bearer";
        public  int NguoiDung_ID { get; set; }
    }
}
