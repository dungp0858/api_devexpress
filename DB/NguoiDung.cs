﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DB
{
    public class NguoiDung 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string? MA_NGUOI_DUNG { get; set; }
        public string? TEN_NGUOI_DUNG { get;set; }
        public string? TEN_DANG_NHAP { get; set; }
        public string? MAT_KHAU { get; set; }
        public string? EMAIL { get; set; }

    }
    public class NguoiDungCollection : TableCollection
    {
        public NguoiDungCollection(DB_CONTEXT db) : base(db) { }

        public IQueryable<NguoiDung> Get()
        {
            return _db.NguoiDung.AsQueryable();
        }
        public NguoiDung GetByTenDangNhap(string strTen_Dang_nhap)
        {
            return Get().FirstOrDefault(c=>c.TEN_DANG_NHAP.ToLower() == strTen_Dang_nhap.ToLower());
        }
        public void Add(NguoiDung item)
        {
            _db.NguoiDung.Add(item);
            _db.SaveChanges();
        }
        public void Update(NguoiDung item, bool save = true)
        {
            _db.NguoiDung.Update(item);
            if (save) _db.SaveChanges();
        }
        public void Remove(NguoiDung item)
        {
            if (item == null) return;
            _db.NguoiDung.Remove(item);
            _db.SaveChanges();
        }
    
    }
}
