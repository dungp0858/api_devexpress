﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB
{
    public class DB_CONTEXT : DbContext
    {
        public DB_CONTEXT(DbContextOptions<DB_CONTEXT> options) : base(options){}

        public DbSet<NguoiDung> NguoiDung { get; set; }
       // public DbSet<Token> Token { get; set; }
        private NguoiDungCollection _NguoiDungCollection { get; set; }
        public NguoiDungCollection NguoiDungCollection
        {
            get
            {
                if (null == _NguoiDungCollection)
                    _NguoiDungCollection = new NguoiDungCollection(this);
                return _NguoiDungCollection;
            }
        }
    }
   
    public class TableCollection
    {
        protected DB_CONTEXT _db;
        public TableCollection(DB_CONTEXT db)
        {
            _db = db;
        }
    }
}
